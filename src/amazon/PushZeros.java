package amazon;

public class PushZeros {

    public static void moveZerosToEnd(int[] input) {
        int count = 0;
        int length = input.length;
        for (int i = 0; i <= length - 1; i++) {
            if (input[i] != 0) {
                input[count] = input[i];
                count++;
            }
        }
        while (count < length - 1) {
            input[count] = 0;
            count++;
        }
        for (int element : input) {
            System.out.print(element + "\t");
        }

    }

    public static void moveZerosToEndTwo(int[] input) {
        int n = input.length - 1;
        for (int i = 0, j = 0; i < n; i++) {
            if (input[i] != 0 && input[j] == 0) {
                input[j] = input[i];
                input[i] = 0;
            }

            if (input[j] != 0) {
                j++;
            }
        }
        for (int element : input) {
            System.out.print(element + "\t");
        }
    }

    public static void main(String[] args) {
        int[] inputArray = {1, 3, 2, 0, 0, 4, 5, 0};
        moveZerosToEnd(inputArray);
        System.out.println();
        moveZerosToEndTwo(inputArray);
    }
}
