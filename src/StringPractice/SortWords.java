package StringPractice;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class SortWords {

    public static int getWordStrength(String word) {
        int weight = 0;
        for (int i = 0; i < word.length(); i++) {
            weight = weight + (int) word.charAt(i);
        }
        return weight;
    }

    public static void main(String[] args) {
        String sentence = "eeee gggg bbbb pppp aaaa ffff mmmm";
        String sortedSentence = "";
        String[] words = sentence.split(" ");
        Map<Integer, String> wordMap = new TreeMap<>(Collections.reverseOrder());
        for (String word : words) {
            wordMap.put(getWordStrength(word), word);
        }
        for (Map.Entry<Integer, String> entry : wordMap.entrySet()) {
            sortedSentence = sortedSentence + " " + entry.getValue();
        }
        System.out.println(sortedSentence);

    }
}
