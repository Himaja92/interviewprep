package StringPractice;

public class RecursionReverse {

    public static String reverse(String input) {
        if (input.isEmpty()) {
            System.out.println("String is empty now");
            return input;
            //this is merge test comment
            //Reverse
        }
        return reverse(input.substring(1)) + input.charAt(0);
    }

    public static void main(String[] args) {
        System.out.println(reverse("Himaja"));
    }
}


