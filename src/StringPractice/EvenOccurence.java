package StringPractice;

import java.util.HashMap;
import java.util.Map;

public class EvenOccurence {
    public static void getEvenCharacters(){
        String test = "MalayalamM";
        HashMap<Character,Integer> counter = new HashMap<>();

        for (int i=0;i<test.length();i++){
            char ch = test.charAt(i);
            if(counter.containsKey(ch)){
                counter.put(ch,counter.get(ch)+1);
            }else{
                counter.put(ch,1);
            }
        }
        for (Map.Entry<Character,Integer> entry : counter.entrySet()){

            if(entry.getValue() %2==0){
                System.out.println(entry.getKey());
            }
        }

    }
    public static void main(String[] args) {
        getEvenCharacters();
    }
}
