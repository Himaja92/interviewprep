package StringPractice;

import javax.security.auth.callback.CallbackHandler;
import java.util.HashSet;

public class CharOccurence {

    public static void getFirstRecurringCharacter(){
        String test = "Banana";
        HashSet<Character> charCount = new HashSet<Character>();
        for (int i=0;i<test.length();i++){
            char ch = test.charAt(i);
            if(!charCount.add(ch)){
                System.out.println("Character is "+ch);
                break;
            }
        }
    }

    public static void main(String[] args) {
        getFirstRecurringCharacter();
    }
}
