package genreral;

public class Iterator {

    public enum Employee {
        JUNIOR(800, 1200),
        MID(1300, 1500),
        SENIOR(1600, 1800),
        MANAGER(2000, 2200);

        private int min;
        private int max;

        Employee(int min, int max) {
            this.min = min;
            this.max = max;
        }

        public static void isValid(Employee employee, int pay) {
            String result;
            result = pay >= employee.min && pay <= employee.max ? "Valid" : "Invalid";

            System.out.println(result);

        }

    }


    public static void main(String[] args) {

        Employee.isValid(Employee.JUNIOR, 900);
        Employee.isValid(Employee.JUNIOR, 1300);
        Employee.isValid(Employee.MID, 1350);
        Employee.isValid(Employee.MID, 900);
        Employee.isValid(Employee.SENIOR, 1700);
        Employee.isValid(Employee.SENIOR, 900);
        Employee.isValid(Employee.MANAGER, 2000);
        Employee.isValid(Employee.MANAGER, 2400);
    }
}
