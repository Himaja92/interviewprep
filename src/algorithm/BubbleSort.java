package algorithm;

public class BubbleSort {

    public static void sort(){
        int[] num = {90,4,32,55,23,78,11,9,6,45};
        int temp=0;

        for(int i=0;i<num.length-1;i++){
            for(int j=0;j<num.length-i-1;j++){
                if(num[j]>num[j+1]){
                    temp = num[j];
                    num[j] = num[j+1];
                    num[j+1] = temp;
                }
            }
        }
        for (int n : num){
            System.out.print(n+"\t");
        }
    }

    public static void main(String[] args) {
        sort();
    }
}
