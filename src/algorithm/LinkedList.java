package algorithm;

public class LinkedList {
    //Head of the linked list
    private Node head = null;

    //Append element to linked list
    public boolean append(String newElementValue) {
        if (head == null) {
            head = new Node(newElementValue);
        } else {
            Node temp = head;
            while (temp.hasNext()) {
                temp = temp.getNextNode();
            }
            temp.setNextNode(new Node(newElementValue));
        }
        return true;
    }

    public boolean addLoopedElement(String newElement){
        Node temp = head;
        while(temp.hasNext()){
            temp = temp.getNextNode();
        }
        Node newNode = new Node(newElement);
        temp.setNextNode(newNode);
        newNode.setNextNode(head);
        return true;
    }

    public boolean append(Node node){
        if (head == null) {
            head = node;
        } else {
            Node temp = head;
            while (temp.hasNext()) {
                temp = temp.getNextNode();
            }
            temp.setNextNode(node);
        }
        return true;

    }

    public Node getFirstNode(){
            return head;
    }
}

    class Node {
        String element;
        Node nextNode;

        //Initialize node
        Node(String value) {
            this.element = value;
            this.nextNode = null;
        }

        public String getValue() {
            return element;
        }

        public void setNextNode(Node node) {
            this.nextNode = node;
        }

        public Node getNextNode(){
            return this.nextNode;
        }

        public boolean hasNext() {
            if (nextNode == null) {
                return false;
            } else {
                return true;
            }
        }


    }

