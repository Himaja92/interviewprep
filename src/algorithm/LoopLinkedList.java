package algorithm;

public class LoopLinkedList {

    public static boolean isListLooped(LinkedList list){

        //get 2 iterators with 1 and 2 jumps
        Node iterator1 = list.getFirstNode();
        Node iterator2 = list.getFirstNode();
        //Iterate throguh the list using hasnext
            while(iterator1.hasNext()){
                iterator1 = iterator1.getNextNode();
                if(iterator1.hasNext()){
                    iterator1 = iterator1.getNextNode();
                }
                iterator2 = iterator2.getNextNode();
                //Compare if the iterators have met at any point
                if(iterator1 .equals(iterator2)){
                    return true;
                }
            }

        return false;
    }

    public static void main(String[] args) {

        LinkedList list = new LinkedList();
        Node node = new Node("6");
        list.append(node);
        list.append(new Node("1"));
        list.append(new Node("3"));
        list.append(new Node("3"));
        list.append(new Node("4"));
        //list.append(node);
        //list.addLoopedElement("6");
        System.out.println(isListLooped(list));

    }

}
