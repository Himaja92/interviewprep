package algorithm;

import java.util.Arrays;

public class ReverseSubsetArray {

    //Reverse array
    public static void reverseArray(int[] input, int startIndex, int endIndex) {
        int temp;
        while (endIndex > startIndex) {
            temp = input[startIndex];
            input[startIndex] = input[endIndex];
            input[endIndex] = temp;
            startIndex += 1;
            endIndex -= 1;
        }
    }

    //Get the subset of array and pass it to reverse array
    public static void getSubsetReverseArray(int[] input, int subSetLength) {
        int length = input.length;
        int endIndex;
        for (int i = 0; i < length - 1; ) {
            endIndex = i + subSetLength - 1;
            if (endIndex >= length) {
                endIndex = length - 1;
            }
            reverseArray(input, i, endIndex);
            i = i + subSetLength;
        }
        System.out.println(Arrays.toString(input));
    }

    public static void main(String[] args) {
        int[] inputArray = {1, 2, 3, 4, 5, 6, 7, 8, 9};
        getSubsetReverseArray(inputArray, 3);
    }
}
