package algorithm;

import java.util.ArrayList;

public class DutchFlag {

    public static void sort(){
        int arr[] = { 0, 1, 1, 0, 1, 2, 1, 2, 0, 0, 0, 1 };
        int low=0,mid=0,high=arr.length-1,temp=0;

        while(mid<=high){
            switch (arr[mid]){
                case 0:
                    temp = arr[low];
                    arr[low] = arr[mid];
                    arr[mid] = temp;
                    low++;
                    mid++;
                    break;
                case 1:
                    mid++;
                    break;
                case 2:
                    temp = arr[high];
                    arr[high] = arr[mid];
                    arr[mid] = temp;
                    high--;
                    break;
            }
        }
        for(int n:arr){
            System.out.print(n+"\t");
        }
    }

    public static void main(String[] args) {
     sort();
    }
}
